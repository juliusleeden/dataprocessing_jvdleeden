
""" 1. REMOTE DATA """

""" 
DOWNLOAD A FILE USING HTTP:
Below you find an example in which the HTTP provider is used to download a 
file from to the site https://bioinf.nl/~fennaf/snakemake/test.txt into a current work directory. 
"""
import os
from snakemake.remote.HTTP import RemoteProvider as HTTPRemoteProvider

HTTP = HTTPRemoteProvider()

rule all:
    input:
        HTTP.remote("bioinf.nl/~fennaf/snakemake/test.txt", keep_local=True)
    run:
        outputName = os.path.basename(input[0])
        shell("mv {input} {outputName}")
        

""" 
RETRIEVE A FILE FROM NCBI: 
The NCBI.remote("file.fasta", db="source database") will retrieve the fasta file 
from the NCBI source database. For example, NCBI.RemoteProvider().remote("file.fasta", 
db="source database") will download a FASTA file while NCBI. RemoteProvider().remote("file.gb", 
db="source database") will download a GenBank-format file.  Below you find an example of the NCBI provider.
"""
import os
from snakemake.remote.NCBI import RemoteProvider as NCBIRemoteProvider
NCBI = NCBIRemoteProvider(email="f.feenstra@pl.hanze.nl") # email required by NCBI

rule all:
    input:
        NCBI.remote("KY785484.1.fasta", db="nuccore")
    run:
        outputName = os.path.basename("test.fasta")
        shell("mv {input} {outputName}")

""" EXCERCISE
Before you start: Make sure that you activate your virtual environment. 
Download first the https://bioinf.nl/~fennaf/snakemake/test.txt file via a Snakefile. 
Once you succeeded download the KY785484.1.fasta file from the nuccore database via a Snakefile. 
If you encounter error message related to packages make sture that you install the required packages 
in your virtual environment. [see code above]
"""


"""
REMOTE ACCESS
Downloading files is not always needed for the pipeline processing. One might be simply read the 
file remotely by using the file as input and store only the outputfile. An example is given below. 
In the example the size of the file is count by the shell command wc -c
"""
from snakemake.remote.NCBI import RemoteProvider as NCBIRemoteProvider
NCBI = NCBIRemoteProvider(email="j.p.van.der.leeden@st.hanze.nl") # email required by NCBI to prevent abuse

rule all:
    input:
        "size.txt"

rule download_and_count:
    input:
        NCBI.remote("AAH32336.1.fasta", db="protein")
    output:
        "size.txt"
    run:
        shell("wc -c {input} > {output}")


"""
Below you find an example of a query searching for Zika virus genomes. 
Searched is the organism Zika Virus with a Sequence length [SLEN] between 9000 and 20000, 
with a publication date [PDAT] between 2017/03/20 and 2017/03/24
"""
from snakemake.remote.NCBI import RemoteProvider as NCBIRemoteProvider
NCBI = NCBIRemoteProvider(email="j.p.van.der.leeden@st.hanze.nl") # email required by NCBI to prevent abuse

# get accessions for the first 3 results in a search for full-length Zika virus genomes
# the query parameter accepts standard GenBank search syntax
query = '"Zika virus"[Organism] AND (("9000"[SLEN] : "20000"[SLEN]) AND ("2017/03/20"[PDAT] : "2017/03/24"[PDAT])) '
accessions = NCBI.search(query, retmax=4)

# give the accessions a file extension to help the RemoteProvider determine the
# proper output type.
input_files = expand("{acc}.fasta", acc=accessions)

rule all:
    input:
        "sizes.txt"

rule download_and_count:
    input:
        # Since *.fasta files could come from several different databases, specify the database here.
        # if the input files are ambiguous, the provider will alert the user with possible options
        # standard options like "seq_start" are supported
        NCBI.remote(input_files, db="nuccore", seq_start=5000)

    output:
        "sizes.txt"
    run:
        shell("wc -c {input} > sizes.txt")

""" EXERCISE
Create a multifasta file from 4 Zika virus genomes [see code above]
"""

""" 2. DATA PATHS """

#CONCATENATION EXAMPLE
wdir = "/data/storix2/student/thema11/jdoe/"
FASTQ_DIR = 'data/'

rule quantify_genes:
    input:
        genome = wdir + 'genome.fa'
        r1 = wdir + FASTQ_DIR + '{sample}.R1.fastq.gz',
        r2 = wdir + FASTQ_DIR + '{sample}.R2.fastq.gz'
    output:
        wdir + '{sample}.txt'
    shell:
        'echo {input.genome} {input.r1} {input.r2} > {output}'
        
#JOIN EXAMPLE
from os.path import join

wdir = "/data/storix2/student/thema11/jdoe/"
FASTQ_DIR = "data/"

rule quantify_genes:
    input:
        genome = join(wdir,'genome.fa')
        r1 = join(wdir, FASTQ_DIR, '{sample}.R1.fastq.gz'),
        r2 = join(wdir, FASTQ_DIR, '{sample}.R2.fastq.gz')
    output:
        '{sample}.txt'
    shell:
        'echo {input.genome} {input.r1} {input.r2} > {output}'

# Best practise of course is not to use a variable wdir for our working directory but the workdir: feature
#concatenation example with workdir
workdir: "/data/storix2/student/thema11/jdoe/"
FASTQ_DIR = 'data/'

rule quantify_genes:
    input:
        genome = 'genome.fa'
        r1 = FASTQ_DIR + '{sample}.R1.fastq.gz',
        r2 = FASTQ_DIR + '{sample}.R2.fastq.gz'
    output:
        '{sample}.txt'
    shell:
        'echo {input.genome} {input.r1} {input.r2} > {output}'

""" EXERCISE:
Move your data from Tutorial 2 to a data directory on your commons directory. 
It is not needed to do this by means of a snakemake script, you just can copy the data. 
Now rewrite the script from tutorial 2 using join function and the workdir. 
Mind you the example above is from tutorial 1, you need to adjust tutorial 2 script

-> For this code see file: WCO3
"""

""" 3. INPUT FILES """

""" CONFIG FILES
Snakemake provides a config file mechanism. Config files can be written in JSON or YAML, and loaded with the configfile directive. 
This mechanism is more flexible since you can use data from several sources.

First we need to create a config.yaml file in the working directory listing the files. 
Instead of expanding a list with the sample names we now expand the samples from the configfile

Data (A.bam .. J.bam) for this example below is to be found at bioinf.nl/~fennaf/snakemake/WC03/data
"""

configfile: "config.yaml"
    
rule merge_variants:
    input:
         fa=config["genome"] + config["ext"],
         fai=config["genome"] + config["ext"] +  ".fai",
         dict=config["genome"] + ".dict",
         vcf=expand("calls/{sample}.g.vcf", sample=config["samples"]),
    output:
        temp("calls/merged_results.vcf")
    message: "Executing GATK CombineGVCFs with {threads} threads on the following files {input}."
    shell:
        "java -jar ./GATK/GenomeAnalysisTK.jar -T CombineGVCFs -R {input.fa} {vcf2} -o {output} "

""" include:
Main script to be executed.
Includes all necessary Snakefiles needed
to execute the workflow.
 """

configfile: "config.yaml"
include: "gatk.snakefile"
include: "fasta.snakefile"
include: "bam.snakefile"
include: "bcftools.snakefile"


rule all:
    input:
        "results/summary.vchk"

 """ retrieve files function
Using configfiles is one way to work flexible input, but a long list of files might be not easy to maintain in a config file. 
One way to deal with that is to simply retrieve the files from the os. In snakemake we can define functions and use such a function as input:
 """

def myfunc(wildcards):
    return [... a list of input files depending on given wildcards ...]

rule:
    input: myfunc
    output: "someoutput.{somewildcard}.txt"
    shell: "..."

""" glob.glob
The glob module finds all the pathnames matching a specified pattern according to the rules used by the Unix shell. 
Using glob.glob is useful in the list comprehension to retrieve files.
 """ 
 
import glob
import os

SAMPLES = [os.path.basename(x).rstrip(".fastq") for x in glob.glob("data/samples/*")]
print(SAMPLES) 

