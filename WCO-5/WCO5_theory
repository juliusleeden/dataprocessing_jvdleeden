"""	SCHEDULING
CPU'S

Before we optimize our code it is important to get an idea about the computers 
capability to execute jobs in parallel. To get an idea what is available on threads, 
cores, sockets and CPU’s can type

"""

lscpu | egrep 'Thread|Core|Socket|^CPU\('

What is the difference between bin201 and nuc410?
nuc410:
$ lscpu | egrep 'Thread|Core|Socket|^CPU\('
CPU(s):                4
Thread(s) per core:    2
Core(s) per socket:    2
Socket(s):             1
Model name:            Intel(R) Core(TM) i5-7260U CPU @ 2.20GHz

bin201:
$ lscpu | egrep 'Thread|Core|Socket|^CPU\('
CPU(s):                4
Thread(s) per core:    1
Core(s) per socket:    4
Socket(s):             1

nuc410 has more threats per core

""" BENCHMARKING
With the benchmark directive, Snakemake can be instructed to measure the wall 
clock time of a job. In theory our job should run faster if we use multiple threads. 
Let us try this. Copy the complete script of Tutorial 2 to a new directory. 
Incorperate the following in rule bwa_map:
"""

rule bwa_map:
    input:
        join(FDIR, "genome.fa"),
        "data/samples/{sample}.fastq"
    output:
        "mapped_reads/{sample}.bam"
    benchmark:
        "benchmarks/{sample}.bwa.benchmark.txt"
    threads: 8
    shell:
        "bwa mem -t {threads} {input} | samtools view -Sb - > {output}"

# See adjusted script from week 2 in WCO2_benchmark

""" THREADS
We have to tell snakemake to use more cores!
""" 
snakemake --cores 4

"""
Report benchmark
You can include the benchmark files in your report for more access convenience. 
Just add a T2 variable.
"""

rule report:
    input:
        T1="calls/all.vcf",
        T2=expand("benchmarks/{sample}.bwa.benchmark.txt", sample=SAMPLES)
    output:
        "out.html"
    run:
        from snakemake.utils import report
        with open(input[0]) as f:
            n_calls = sum(1 for line in f if not line.startswith("#"))

        report("""
        An example workflow
        ===================================

        Reads were mapped to the Yeast
        reference genome and variants were called jointly with
        SAMtools/BCFtools.

        This resulted in {n_calls} variants (see Table T1_).
        Benchmark results for BWA can be found in the tables T2_.
        """, output[0], **input)

""" LOGGING
In the script you saw a log directive. When executing a large workflow, 
it is usually desirable to store the output of each job persistently in 
files instead of just printing it to the terminal. For this purpose, 
Snakemake allows to specify log files for rules. Log files are defined 
via the log directive and handled similarly to output files, but they 
are not subject of rule matching and are not cleaned up when a job fails. 
We modify our rule bwa_map as follows:
"""

log:
	"logs/bwa_mem/{sample}.log"
threads: 8
shell:
	"(bwa mem  -t {threads} {input} | "
	"samtools view -Sb - > {output}) 2> {log}"

"""
The shell command is modified to collect STDERR output of both bwa and samtools 
and pipe it into the file referred by {log}. Log files must contain exactly 
the same wildcards as the output files to avoid clashes. It is best practice 
to store all log files in a subdirectory logs/, prefixed by the rule or tool name.

This logging is especially useful to check afterwards if your script runned 
as expected. After execution you can check all the updated files output files 
and logfiles with the command
"""

snakemake --summary

""" EXERCISE
Incorperate for every data processing rule an error log with the log directive. 
Run your script. When it is finnished run
"""

# ADDED TO WCO5
""" Result of --summary:
Building DAG of jobs...
output_file     date    rule    version log-file(s)     status  plan
output/out.html Sat Apr  6 21:15:03 2019        report  -       logs/report/out.log     ok      no update
output/calls/all.vcf    Sat Apr  6 21:14:59 2019        bcftools_call   -       logs/bcftools_call/all.vcf.log ok       no update
output/mapped_reads/B.bam       Sat Apr  6 21:15:00 2019        bwa_map -       logs/bwa_mem/B.log      ok     no update
output/mapped_reads/A.bam       Sat Apr  6 21:15:02 2019        bwa_map -       logs/bwa_mem/A.log      ok     no update
output/mapped_reads/C.bam       Sat Apr  6 21:15:01 2019        bwa_map -       logs/bwa_mem/C.log      ok     no update
"""


