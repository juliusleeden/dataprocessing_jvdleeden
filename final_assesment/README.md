# Dataprocessing final assessment

In this repository you can find all scripts, files, and information needed to run the snakemake script for my final assignment of the dataprocessing class 2018/2019.  
This script can be used to map samples against a reference genome to search for any overlapping reads. And will return to the user text files containing all of the overlapping reads and their scientific name.

# Getting Started
## Prerequisites
In order to run the script the user needs to have the following tools/packages.

- snakemake
- python3
- samtools
- bedtools

A config.yaml file has been included to store all of the sample names and location, and reference fasta location. If a user wants to use different samples they can adjust here as needed. They current config file contains data found on:

- virus reference: assemblix: /data/storage/dataprocessing/
- dutch microglia samples: assemblix: /data/storage/dataprocessing/rnaseq_data/Dutch_Microglia/

# Rules
The pipeline will execute the following rulesin order if the output file does not already exist. If the output file does exist the step will be skipped.

- **create_index:** Creates a index from the reference fasta file in the config.yaml
"bowtie2-build {input} {params.basename} --large-index"
- **run_bowtie2_against_virus:** Creates SAM files from mapping the sample reads against the indexed reference.
"(bowtie2 -x bowtie2/virus -1 {input.fastq1} -2 {input.fastq2} -S {output} -p {threads} --very-fast) 2> {log}".  
- **convert_sam_to_bam:** Converts the SAM files from the previous rule into BAM files.
"(samtools view -@ {threads} -b -S -o {output} {input}) 2> {log}"
- **remove_failed_to_align:** Removes any reads that failed to properly align to the reference from the BAM files.
"(samtools view -@ {threads} -b -F 4 {input} > {output}) 2> {log}"
- **get_genome_accession_numbers:** Retrieves the genome accession numbers from the BAM files and returns them in a readable text file.
"(bedtools bamtobed -i {input} > {output}) 2> {log}"
- **create_genome_dict:** Creates a text file containing accession numbers/scientific name combinations for all the reads in the reference fasta.
Uses a custom script which can be found at /scripts/get_genomes.py
- **accession_to_name:** Uses the genomic dict to translate all accession numbers per sample file to scientific names.
Uses a custom script which can be found at /scripts/accessions_to_name.py
- **Report:** Will return custom HTML pages containing the results and links to the data.

# Extra
- View the dag.png for the global layout of the snakemake script.
- If creating an index file is taking to long, try the virus reference index in the data folder (bowtie2.zip).
- The project this pipeline was based on can be found under resources.
- There is a condor bash script available under the condor folder. This can be used to easily start a condor job using the pipeline script.

### Author
Julius van der Leeden (308533)
j.p.van.der.leeden@st.hanze.nl
