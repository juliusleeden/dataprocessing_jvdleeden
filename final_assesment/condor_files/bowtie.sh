#!/bin/bash
source /homes/jpvanderleeden/Desktop/dataprocessing/snakemake-test/venv/bin/activate
snakemake --snakefile snakemake_pipeline --cores 16
deactivate
