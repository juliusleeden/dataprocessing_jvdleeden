""" 
Name:       run_bowtie2_against_virus
Author:     Julius vd Leeden
Date:       14-04-2019
Purpose:    Runs sample fasta files against the virus index and return SAM files with results.
"""

rule run_bowtie2_against_virus:
    input:
        expand("bowtie2/virus.{index}.bt2l", index=range(1, 5)),
        expand("bowtie2/virus.rev.{index}.bt2l", index=range(1, 3)),
        
        fastq1 = config["samples_dir"] + "{sample}_R1.fastq",
        fastq2 = config["samples_dir"] + "{sample}_R2.fastq"
    output:
        temp("mapped_reads/mapped_sam/{sample}.sam")
    message:
        "Mapping {wildcards.sample} against Virus index"
    threads:
        40
    log:
        "logs/run_bowtie2_against_virus/{sample}.log"
    shell:
        "(bowtie2 -x bowtie2/virus -1 {input.fastq1} -2 {input.fastq2} -S {output} -p {threads} --very-fast) 2> {log}"
