#!/usr/bin/python3

# Open output file
output = open(snakemake.output[0], "w")

# If a key overlaps write it to the outputfile.
for line in open(snakemake.input[0], "r"):
	for sample_line in open(snakemake.input[1], "r"):
		sample = sample_line.split()[0].strip("| ")
		if sample == line.split(",")[0]:
			output.write(line + "\n")
			
output.close()
