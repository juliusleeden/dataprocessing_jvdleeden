#!/usr/bin/python3

"""
Script from iris gorter.
Adjusted for usage with snakemake.
Get all genome accession numbers in a dictionary and save them in a csv file.
"""

# input[0] = all_virus.fna
pathogen_fasta = snakemake.input[0]

genomedict = {}
with open(pathogen_fasta) as AllGenomes:
	for line in AllGenomes:
		if line.startswith(">gi"):
			genome = line.split(">")[1].split(",")[0]
			refname = genome.split("| ")[0]
			organism = genome.split("| ")[1]
			genomedict[refname] = organism
	 
		elif line.startswith(">JPKZ") or line.startswith(">MIEF") or line.startswith(">LL") or line.startswith(">AWXF") or line.startswith("EQ") or line.startswith(">NW_") or line.startswith(">LWMK") or line.startswith(">NZ_") or line.startswith(">NC_") or line.startswith(">KT"):
			genome = line.split(">")[1].split(",")[0]
			refname = genome.split(" ")[0]
			organismName = genome.split(" ")[1:]
			organism = ' '.join(organismName)
			genomedict[refname] = organism 

# Save the dictionary in a csv file defined by snakemake output.
with open(snakemake.output[0], 'w') as f:
	for key in genomedict.keys():
		f.write("%s,%s\n"%(key,genomedict[key]))
